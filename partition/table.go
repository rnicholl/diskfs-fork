package partition

import (
	"gitlab.com/rnicholl/diskfs-fork/partition/part"
	"gitlab.com/rnicholl/diskfs-fork/util"
)

// Table reference to a partitioning table on disk
type Table interface {
	Type() string
	Write(util.File, int64) error
	GetPartitions() []part.Partition
}
