package diskfs

import (
	"fmt"
	"os"

	"golang.org/x/sys/unix"
)

// getBlockDeviceSize get the size of an opened block device in Bytes.
func getBlockDeviceSize(f *os.File) (int64, error) {
	blockDeviceSize, err := unix.IoctlGetInt(int(f.Fd()), unix.BLKGETSIZE64)
	if err != nil {
		return 0, fmt.Errorf("unable to get block device size: %v", err)
	}
	return int64(blockDeviceSize), nil
}
